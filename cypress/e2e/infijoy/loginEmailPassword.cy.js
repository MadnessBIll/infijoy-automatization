describe("infijoyLogin", () => {
  beforeEach(() => {
    cy.visit("https://app-staging.infijoy-dev.com/sign-in");
  });

  it("login", () => {
    cy.wait(1000);
    cy.get("button.ab-close-button").click();
    cy.get("#firstName").type("testmailpurr+auto@gmail.com");
    cy.get("#password").type("Qwerty123");
    cy.get("button").contains("Log in").click();
  });
});
