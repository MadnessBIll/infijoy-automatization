function randomNumberGenerator(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);

  return Math.floor(Math.random() * (max - min + 1) + min);
}
const nameArray = ["John", "Sarah", "Alexey", "Masha"];
const surnameArray = ["Banjo", "Petrov", "Obame", "Tree"];
const countryArray = ["Russia", "USA", "Congo", "Albania"];

const stringPicker = randomNumberGenerator(0, 3);
const emailNumber = randomNumberGenerator(1, 250);

describe("infijoyLogin", () => {
  beforeEach(() => {
    cy.visit("https://app-staging.infijoy-dev.com/sign-up");
    sessionStorage.clear();
  });

  it("registrationEmailPassword", () => {
    const randomName = nameArray[stringPicker];
    const randomSurname = surnameArray[stringPicker];
    const randomEmail = nameArray[stringPicker] + surnameArray[stringPicker];
    const validateEmail = `testmailpurr+${randomEmail}${emailNumber}@gmail.com`;

    cy.wait(1000);
    cy.get("button.ab-close-button").click();

    cy.get("*[class^=SignUp__FormStyled]").within(() => {
      cy.get("input").eq(0).type(randomName);
      cy.get("input").eq(1).type(randomSurname);
      cy.get("input").eq(3).type(validateEmail);
      cy.get("input").eq(2).click().type("Russia{enter}");

      cy.get("[class^=Checkbox__CheckboxIcon]").click();
    });
  });
});
